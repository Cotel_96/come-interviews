public class StringTransformer {


    /** Given a string this method returns its reversed version.
     *
     *  For example: reverse("Hello") => "olleH"
     *
     * @param string - The original string
     * @return A reversed version of the original string
     */
    public String reverse(String string) {
        return string;
    }

    /** Given a string this method returns its camel cased version.
     *
     *  For example: camelCase("Hello my friend") => "helloMyFriend"
     *
     * @param string - The original string
     * @return A camel cased version of the original string
     */
    public String camelCase(String string) {
        return string;
    }

    /** Given a string this method first transforms it to a camel cased
     *  version and then transforms it to a snake cased version.
     *
     *  For example: snakeCase("Hello my friend") => "hello_my_friend"
     *
     *  YOU MUST FOLLOW THIS RESTRICTION!
     *
     * @param string - The original string
     * @return A snake cased version of the original string
     */
    public String snakeCase(String string) {
        String camelCased = camelCase(string);

        return camelCased;
    }

    /** Given a string this method returns its palindrome.
     *
     *  For example: palindrome("Hello") => "HelloolleH"
     *
     * @param string - The original string
     * @return A palindrome built from the original string
     */
    public String palindrome(String string) {
        return string;
    }

}
