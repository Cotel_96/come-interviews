import junit.framework.Assert.assertEquals
import org.junit.Test

class StringTransformerTest {

    private val transformer = StringTransformer()

    @Test
    fun `reverse string should return the reversed string`() {
        val originalString = "Hello how are you?"
        val expectedString = "?uoy era woh olleH"

        val result = transformer.reverse(originalString)

        assertEquals(expectedString, result)
    }

    @Test
    fun `camel case string should return the camel cased version`() {
        val originalString = "Original String"
        val expectedString = "originalString"

        val result = transformer.camelCase(originalString)

        assertEquals(expectedString, result)
    }

    @Test
    fun `snake case string should return the snake cased version`() {
        val originalString = "Original String"
        val expectedString = "original_string"

        val result = transformer.snakeCase(originalString)

        assertEquals(expectedString, result)
    }

    @Test
    fun `palindrome should return a palindrome from a string`() {
        val originalString = "Original String"
        val expectedString = "Original StringgnirtS lanigirO"

        val result = transformer.palindrome(originalString)

        assertEquals(expectedString, result)
    }

}
